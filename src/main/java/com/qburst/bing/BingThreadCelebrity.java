package com.qburst.bing;

import org.apache.log4j.Logger;

public class BingThreadCelebrity extends Thread {
    private String url;
    private Logger logger;

    public BingThreadCelebrity(String url) {
        this.url = url;
        logger = Logger.getLogger(BingThreadCelebrity.class);
    }

    public void run() {
        CelebrityFurtherLink celebrityFurtherLink = new CelebrityFurtherLink(BingApplication.getContext());
        celebrityFurtherLink.getHandle(url, "facebook");
        celebrityFurtherLink.getHandle(url, "twitter");
        logger.info("celebrity:  " + url);
    }
}
