package com.qburst.bing;

import org.apache.flume.Context;

import java.util.ArrayList;
import java.util.List;

public class Config {


    private List<String> url;
    private List<String> key;
    private String redisPassword;
    private String redisHost;
    private int redisPort;

    public Config(Context context) {

        url = new ArrayList<>();
        url.add(context.getString("url1"));
        url.add(context.getString("url2"));


        redisHost=context.getString("redishost");
        redisPassword=context.getString("redispassword");
        redisPort=context.getInteger("redisport");

        key = new ArrayList<>();
        String keystring = context.getString("key");
        String keys[] = keystring.split(",");
        for (String k : keys) {
            key.add(k);
        }
    }

    public List<String> getUrl() {
        return url;
    }

    public void setUrl(List<String> url) {
        this.url = url;
    }


    public List<String> getKey() {
        return key;
    }

    public void setKey(List<String> key) {
        this.key = key;
    }
    public String getRedisPassword() {
        return redisPassword;
    }

    public void setRedisPassword(String redisPassword) {
        this.redisPassword = redisPassword;
    }

    public int getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(int redisPort) {
        this.redisPort = redisPort;
    }

    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }
}
