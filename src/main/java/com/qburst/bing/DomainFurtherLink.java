package com.qburst.bing;

import org.apache.flume.Context;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import redis.clients.jedis.Jedis;
import org.apache.commons.codec.binary.Base64;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


public class DomainFurtherLink {

    static List<String> twitterDomains = new ArrayList<>();
    static List<String> facebookDomains = new ArrayList<>();
    private Context context;

    public DomainFurtherLink(Context context) {
        this.context = context;
    }

    public List<String> getFacebookDomains() {
        return facebookDomains;
    }

    public void setFacebookDomains(List<String> facebookDomains) {
        this.facebookDomains = facebookDomains;
    }

    public List<String> getTwitterDomains() {
        return twitterDomains;
    }

    public void setTwitterDomains(List<String> twitterDomains) {
        this.twitterDomains = twitterDomains;
    }


    void getHandle(String url, String siteName) {
        Logger logger = Logger.getLogger("FeedParser");
        String query = "";
        String bingUrl = "";
        String accountKeyEnc = "";
        String accountKey = "";

        Config config = new Config(context);
        if (siteName.equals("facebook")) {
            accountKey = config.getKey().get(0);
        } else {
            accountKey = config.getKey().get(1);

        }

        byte[] accountKeyBytes = Base64.encodeBase64((accountKey + ":" + accountKey).getBytes());
        accountKeyEnc = new String(accountKeyBytes);
        query = url;

        if (url.startsWith("www.") || url.startsWith("in.") || url.startsWith("m.")) {
            int i = url.indexOf(".");
            query = query.substring(i + 1);

        }

        if (query.endsWith(".net") || query.endsWith(".org") || query.endsWith(".in") || query.endsWith(".com")) {
            int i = url.lastIndexOf(".");
            query = query.substring(0, i);
        }
        query = query + "+" + siteName + "%";
        bingUrl = config.getUrl().get(0) + query + config.getUrl().get(1);
        URL url1 = null;
        try {
            String inputLine;
            url1 = new URL(bingUrl);
            URLConnection urlConnection = url1.openConnection();
            urlConnection.setRequestProperty("Authorization", "Basic " + accountKeyEnc);

            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            while ((inputLine = br.readLine()) != null) {

                JSONObject json = null;
                JSONArray results = new JSONArray();
                try {
                    json = new JSONObject(inputLine);
                    JSONObject d = json.getJSONObject("d");
                    results = d.getJSONArray("results");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                for (int i = 0; i < results.length(); i++) {
                    String jsonurl = null;
                    try {
                        jsonurl = results.getJSONObject(i).getString("Url");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    URL url2 = new URL(jsonurl);
                    if (siteName.equals("facebook")) {
                        if (url2.getHost().contains("facebook.com")) {
                            int index = jsonurl.indexOf("facebook.com");
                            index += 13;
                            jsonurl = jsonurl.substring(index);
                            if (!jsonurl.contains("/") && !jsonurl.contains("?")) {
                                if (!jsonurl.isEmpty()) {
                                    synchronized (facebookDomains) {
                                        facebookDomains.add(jsonurl);
                                    }
                                }
                            }
                        }
                    }
                    if (siteName.equals("twitter")) {
                        if (url2.getHost().equals(siteName + ".com")) {

                            jsonurl = jsonurl.replace("http://www.twitter.com/", "");
                            jsonurl = jsonurl.replace("https://www.twitter.com/", "");
                            jsonurl = jsonurl.replace("https://twitter.com/", "");
                            jsonurl = jsonurl.replace("http://twitter.com/", "");
                            int index = jsonurl.indexOf("/");
                            if (jsonurl.contains("/")) {
                                jsonurl = jsonurl.substring(0, index);
                            }
                            if (!jsonurl.isEmpty()) {
                                synchronized (twitterDomains) {
                                    twitterDomains.add(jsonurl);
                                }
                            }
                        }
                    }
                }
                try (Jedis jedis = BingApplication.getPool().getResource()) {
                    jedis.sadd("processedDomains", url);

                }
                br.close();

            }

        } catch (MalformedURLException e) {

        } catch (IOException e) {

        }

    }
}
