package com.qburst.bing;

import org.apache.commons.codec.binary.Base64;
import org.apache.flume.Context;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import redis.clients.jedis.Jedis;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class CelebrityFurtherLink {
    public static int getCount() {
        return count;
    }

    static int count = 0;
    private static final Object lock = new Object();
    private Context context;

    public CelebrityFurtherLink(Context context) {
        this.context = context;
    }

    void getHandle(String url, String siteName) {

        Logger logger = Logger.getLogger("FeedParser");
        String query = "";
        String bingUrl = "";
        String accountKeyEnc = "";
        String accountKey = "";
        List<String> stfb = new ArrayList<>();
        List<String> sttw = new ArrayList<>();
        Config config = null;
        config = new Config(context);
        if (siteName.equals("facebook")) {
            accountKey = config.getKey().get(2);
        } else {
            accountKey = config.getKey().get(3);

        }
        byte[] accountKeyBytes = Base64.encodeBase64((accountKey + ":" + accountKey).getBytes());
        accountKeyEnc = new String(accountKeyBytes);
        query = url;
        query = query.replace(" ", "%20");
        query = query + "%20" + siteName;
        System.out.println(query);
        bingUrl = config.getUrl().get(0) + query + config.getUrl().get(1);
        URL url1 = null;
        try {
            String inputLine;
            url1 = new URL(bingUrl);
            URLConnection urlConnection = url1.openConnection();
            urlConnection.setRequestProperty("Authorization", "Basic " + accountKeyEnc);

            BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while ((inputLine = br.readLine()) != null) {
                JSONObject json = null;
                JSONArray results = new JSONArray();
                try {
                    json = new JSONObject(inputLine);
                    JSONObject d = json.getJSONObject("d");
                    results = d.getJSONArray("results");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                for (int i = 0; i < results.length(); i++) {
                    String jsonurl = null;
                    try {
                        jsonurl = results.getJSONObject(i).getString("Url");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    URL url2 = new URL(jsonurl);
                    if (siteName.equals("facebook")) {
                        if (url2.getHost().contains("facebook.com")) {
                            int index = jsonurl.indexOf("facebook.com");
                            index += 13;
                            jsonurl = jsonurl.substring(index);
                            if (!jsonurl.contains("/") && !jsonurl.contains("?")) {
                                if (!jsonurl.isEmpty()) {
                                    synchronized (stfb) {
                                        stfb.add(jsonurl);
                                    }
                                }
                            }
                        }
                    }
                    if (siteName.equals("twitter")) {
                        if (url2.getHost().equals(siteName + ".com"))

                        {

                            jsonurl = jsonurl.replace("http://www.twitter.com/", "");
                            jsonurl = jsonurl.replace("https://www.twitter.com/", "");
                            jsonurl = jsonurl.replace("https://twitter.com/", "");
                            jsonurl = jsonurl.replace("http://twitter.com/", "");
                            if (!jsonurl.isEmpty()) {
                                synchronized (sttw) {
                                    sttw.add(jsonurl);
                                    synchronized (lock) {
                                        logger.info("count now " + count);
                                        count++;
                                    }
                                }
                            }
                        }
                    }
                }
                for (int i = 0; i < stfb.size(); i++) {
                    try (Jedis jedis = BingApplication.getPool().getResource()) {
                        jedis.sadd(url + "Fb", stfb.get(i));
                    }
                }


                for (int i = 0; i < sttw.size(); i++) {
                    try (Jedis jedis = BingApplication.getPool().getResource()) {
                        jedis.sadd(url + "Tw", sttw.get(i));
                    }
                }


            }
            try (Jedis jedis = BingApplication.getPool().getResource()) {
                jedis.sadd("processedCelebs", url);

            }
            br.close();

        } catch (MalformedURLException e) {

        } catch (IOException e) {
            System.out.print(e);

        }

    }
}
