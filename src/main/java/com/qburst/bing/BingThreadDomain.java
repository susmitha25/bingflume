package com.qburst.bing;


import org.apache.log4j.Logger;

public class BingThreadDomain implements Runnable {
    private String url;
    private Logger logger;


    public BingThreadDomain(String url) {
        this.url=url;
        logger = Logger.getLogger(BingThreadCelebrity.class);
    }

    public void run(){
        DomainFurtherLink domainFurtherLink = new DomainFurtherLink(BingApplication.getContext());
        domainFurtherLink.getHandle(url, "facebook");
       // domainFurtherLink.getHandle(url, "twitter");
        logger.info("domain:  "+url);
    }
}
