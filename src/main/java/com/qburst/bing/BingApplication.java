package com.qburst.bing;

import org.apache.flume.Context;
import org.apache.flume.EventDrivenSource;
import org.apache.flume.channel.ChannelProcessor;
import org.apache.flume.conf.Configurable;
import org.apache.flume.source.AbstractSource;
import org.apache.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class BingApplication extends AbstractSource implements EventDrivenSource, Configurable {

    private static JedisPool pool;
    static Logger logger = Logger.getLogger("FeedParser");
    private static Context context;

    @Override
    public void configure(Context context) {
        this.context = context;
    }

    public static JedisPool getPool() {return pool;}


    public static Context getContext() {
        return context;
    }

    @Override
    public void start() {
        logger.info("Program started");
        Config config = new Config(context);
        Set<String> sCount = null;
        Set<String> domains = null;
        Set<String> celebrities = null;

        int fCount;

        pool = new JedisPool(new JedisPoolConfig(), config.getRedisHost(),config.getRedisPort(), 2000, config.getRedisPassword());
        logger.info("Redis Pool initialized");

        try (Jedis jedis = BingApplication.getPool().getResource()) {
            domains = jedis.smembers("domains");
        }
        logger.info("Domains read");

        try (Jedis jedis = BingApplication.getPool().getResource()) {
            celebrities = jedis.smembers("celebrities");
        }
        logger.info("Celebs read");

        Links domainlinks = new Links(domains);
        Links celebrityLinks = new Links(celebrities);
        DomainFurtherLink domainFurtherLink = new DomainFurtherLink(context);

        try {
            domainlinks.init();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        domainlinks.findUrl("domains");
        domainlinks.finish();
        List<String> fdomain = domainFurtherLink.getFacebookDomains();
        logger.info("facebook domains are being Written");

        try (Jedis jedis = BingApplication.getPool().getResource()) {
            String ar[] = fdomain.toArray(new String[fdomain.size()]);
            for (String a : ar) {
                jedis.sadd("facebookDomains", a);
            }
        }
        logger.info("twitter Domains are being written");

        List<String> tdomain = domainFurtherLink.getTwitterDomains();
        try (Jedis jedis = BingApplication.getPool().getResource()) {
            String ar[] = tdomain.toArray(new String[tdomain.size()]);
            for (String a : ar) {
                jedis.sadd("twitterDomains", a);
            }
        }


        try {
            domainlinks.init();
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        }
        celebrityLinks.findUrl("celebrities");
        domainlinks.finish();
        logger.info("celebrities have been written");
        try (Jedis jedis = BingApplication.getPool().getResource()) {
            sCount = jedis.smembers("twitterCount");
        }
        Iterator<String> it = sCount.iterator();
        logger.info(sCount + " inital count");
        fCount = Integer.parseInt(it.next()) + CelebrityFurtherLink.getCount();
        try (Jedis jedis = BingApplication.getPool().getResource()) {
            jedis.del("twitterCount");
            jedis.sadd("twitterCount", String.valueOf(fCount));
        }
        logger.info(fCount + " final count");
        logger.info("program ended");

        try {
            logger.info("Sleeping for 30 minutes");
            TimeUnit.MINUTES.sleep(30);
            start();
        } catch (InterruptedException e) {
            logger.warn(e.getMessage());
        }
    }


}
