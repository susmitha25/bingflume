package com.qburst.bing;

import redis.clients.jedis.Jedis;

import java.io.*;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.*;

public class Links {
    Set<String> domains;

    public Links(Set<String> domains) {
        this.domains = domains;
    }

    static int maxCoreThread = 10;
    static int maxPoolSize = 20;
    static ThreadPoolExecutor bingExecutor;

    public static void init() throws FileNotFoundException {

        BlockingQueue<Runnable> blockingQueue = new ArrayBlockingQueue<Runnable>(500);
        bingExecutor = new ThreadPoolExecutor(maxCoreThread, maxPoolSize, 50, TimeUnit.MICROSECONDS, blockingQueue);
        bingExecutor.setRejectedExecutionHandler(new RejectedExecutionHandler() {
            @Override
            public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                try {
                    Thread.sleep(250);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                executor.execute(r);
            }
        });
    }

    public static void finish() {
        bingExecutor.shutdown();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (bingExecutor.getActiveCount() > 0) ;
    }

    void findUrl(String filename) {

        Set<String> processedDomains = null;
        try (Jedis jedis = BingApplication.getPool().getResource()) {
            processedDomains = jedis.smembers("processedDomains");
        }
        Set<String> processedCelebs = null;
        try (Jedis jedis = BingApplication.getPool().getResource()) {
            processedCelebs = jedis.smembers("processedCelebs");
        }
        Iterator<String> it = domains.iterator();
        for (int i = 0; i < domains.size(); i++) {
            String url = it.next();
            if (filename.equals("domains")) {
                if (!processedDomains.contains(url)) {

                    bingExecutor.execute(new BingThreadDomain(url));
                }
            }
            if (filename.equals("celebrities")) {
                if (!processedCelebs.contains(url)) {
                    bingExecutor.execute(new BingThreadCelebrity(url));
                }
            }
        }
    }
}
